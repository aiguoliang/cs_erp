package org.xiaoai.other;

public class TestString {

	public static void main(String[] args) {

		System.out.println("主线程名称："+Thread.currentThread().getName()+"开始执行.....");
		MyThread myThread = new MyThread();
		Thread t1 = new Thread(myThread,"线程A");
		Thread t2 = new Thread(myThread,"线程B");
		
		t1.start();
		t2.start();
		System.out.println("主线程名称："+Thread.currentThread().getName()+"开始结束.....");
	}

}
