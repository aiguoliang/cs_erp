package org.xiaoai.other;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataInputStreamTest {

	public static void main(String[] args) {
		try {
			File file = new File("a.jpg");
			File file2 = new File("b.jpg");
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			DataOutputStream out = new DataOutputStream(new FileOutputStream(file2));

			// 1K的数据缓冲
			int len;
			byte[] bs = new byte[1024];
			while ((len = in.read(bs)) != -1) {
				out.write(bs, 0, len);
			}

			in.close();
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
