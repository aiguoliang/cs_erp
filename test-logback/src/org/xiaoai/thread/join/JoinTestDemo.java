package org.xiaoai.thread.join;

public class JoinTestDemo {

	public static void main(String[] args) {
		String threadName = Thread.currentThread().getName();
		System.out.println(threadName + " start.");
		CustomThread1 t1 = new CustomThread1();
		CustomThread2 t2 = new CustomThread2(t1);
		try {
			t1.start();
			Thread.sleep(2000);
			t2.start();
			t2.join(); // 在代碼2里，將此處注釋掉
		} catch (Exception e) {
			System.out.println("Exception from main");
		}
		System.out.println(threadName + " end!");
	}

}
