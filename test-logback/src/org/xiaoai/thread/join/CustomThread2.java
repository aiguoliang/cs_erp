package org.xiaoai.thread.join;

public class CustomThread2 extends Thread {

	CustomThread1 t1;

	public CustomThread2(CustomThread1 t1) {
		this.t1 = t1;
	}

	public void run() {
		String threadName = Thread.currentThread().getName() + "22";
		System.out.println(threadName + " start.");
		try {
			t1.join();
			System.out.println(threadName + " end.");
		} catch (Exception e) {
			System.out.println("Exception from " + threadName + ".run");
		}
	}

}
