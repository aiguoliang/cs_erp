package org.xiaoai.thread.join.in;

public class ThreadTest extends Thread {

	 Thread thread;  
	  
	    public ThreadTest(Thread thread) {  
	        this.thread = thread;  
	    }  
	  
	    @Override  
	    public void run() {  
	        synchronized (thread) {  
	            System.out.println("getObjectLock");  
	            try {  
	                Thread.sleep(90000);  
	            } catch (InterruptedException ex) {  
	             ex.printStackTrace();  
	            }  
	            System.out.println("ReleaseObjectLock");  
	        }  
	    }  
	
	
}
