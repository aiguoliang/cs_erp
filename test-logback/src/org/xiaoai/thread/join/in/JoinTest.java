package org.xiaoai.thread.join.in;

public class JoinTest {

	  public static void main(String[] args) {  
          Thread t = new Thread(new RunnableImpl());  
          ThreadTest t_test =  new ThreadTest(t);
          t_test.start();
          t.start();  
          try {  
              t.join(1000);  
              System.out.println("joinFinish");  
          } catch (InterruptedException e) {  
              e.printStackTrace();           
          }  
      }  
	
	
}
