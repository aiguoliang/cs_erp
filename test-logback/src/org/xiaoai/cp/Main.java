package org.xiaoai.cp;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;

public class Main {

	public static void main(String[] args) {
		
		
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(50);
		
		ExecutorService executor = Executors.newFixedThreadPool(5, new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				
				Thread t = new Thread(r);
				//普通用户线程
				t.setDaemon(false);
				//优先级设置，1-10优先级从低到高
			    t.setPriority(6);
			    
				return t;
			}
		}); 
		
		
		Producer<String>  p1 = new Producer<String>(queue);
		Producer<String>  p2 = new Producer<String>(queue);
		executor.submit(p1);
		executor.submit(p2);
		
		Consumer<String> c1 = new Consumer<String>(queue);
		Consumer<String> c2 = new Consumer<String>(queue);
		Consumer<String> c3 = new Consumer<String>(queue);
		executor.submit(c1);
		executor.submit(c2);
		executor.submit(c3);
		try {
			Thread.sleep(200000);
			p1.stop();
			p2.stop();
			c1.stop();
			c2.stop();
			c3.stop();
			executor.shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	
}
