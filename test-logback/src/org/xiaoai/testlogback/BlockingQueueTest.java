package org.xiaoai.testlogback;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueTest {

	public static void main(String[] args) throws InterruptedException {
		// 声明一个容量为10的缓存队列
		BlockingQueue queue = new LinkedBlockingQueue(10);

		Producer producer1 = new Producer(queue);
		Producer producer2 = new Producer(queue);
		Producer producer3 = new Producer(queue);
		Producer producer4 = new Producer(queue);
		Producer producer5 = new Producer(queue);
		Consumer consumer = new Consumer(queue);

		// 借助Executors
		ExecutorService service = Executors.newCachedThreadPool();

		// ExecutorsService ex = Executors.newCachedThreadPool();

		// 启动线程
		service.execute(producer1);
		producer1.stop();
		service.execute(producer2);
		service.execute(producer3);
		service.execute(producer4);
		service.execute(producer5);
		service.execute(consumer);

		
		//测试
		
		
		// 执行10s
		Thread.sleep(10 * 1000);

		producer2.stop();
		producer3.stop();
		producer4.stop();
		producer5.stop();

		Thread.sleep(2000);
		// 退出Executor
		service.shutdown();

	}

}
