package org.xiaoai.testlogback;

import java.util.concurrent.ArrayBlockingQueue;

public class TestArrayBlockingQueue {

	public static void main(String[] args) {
		
		// ArrayBlockingQueue<String> abq = new ArrayBlockingQueue<String>(10);  
		 
		
      try {
		insertBlocking();
		 fetchBlocking(); 
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  
    
		
		
	}
	
    /** 
     * 此方法展示了 ArrayBlockingQueue 的插入阻塞特性 ：如果队列已经满了，那么插入的操作就会被阻塞，程序执行就会被迫暂停。 
     */  
    public static void insertBlocking() throws InterruptedException {  
        ArrayBlockingQueue<String> names = new ArrayBlockingQueue<String>(1);  
        names.put("a");  
        // 从这一句开始后面的就不会被执行了  
        names.put("b");  
          
        System.out.println("程序执行到此...");  
    }
    
    
    
    /** 
     * 此方法展示了 ArrayBlockingQueue 的取出阻塞特性 ：如果队列为空，那么取的操作就会被阻塞，程序执行就会报错。 
     *  
     */  
    public static void fetchBlocking() throws InterruptedException {  
        ArrayBlockingQueue<String> names = new ArrayBlockingQueue<String>(1);  
        names.put("a");  
        names.remove();  
        names.remove();  
        names.put("b");  
          
        System.out.println("程序执行到此...");  
    }  
	
	
	
}
